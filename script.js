/*
* Экранирование - способ использовать специальные символы языка в качестве строковых. Язык программирования
*  распознает свои специальные символы как команду к действию. Для того, чтобы специальный символ читался
* языком, как строковый, необходимые дополнительные символы - символы экранирования. В js это обратный слэш
* или двойной обратный слэш в случае с newRegExp()* */

function CreateNewUser() {
    const firstName = prompt('What is your name?', '');
    const lastName = prompt('What is your lastname?', '');
    const birthday = prompt('Enter the date of your birth', 'dd.mm.yyyy');

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        _birthday: birthday,

        get firstName() {
            return this._firstName
        },

        set firstName(firstName) {
            this._firstName = firstName
        },

        get lastName() {
            return this._lastName
        },

        set lastName(lastName) {
            this._lastName = lastName
        },

        get birthday() {
            return this._birthday
        },

        set birthday(birthday) {
            this._birthday = birthday
        },

        getAge() {
            const dateNow = new Date();
            const currentYear = dateNow.getFullYear();
            return currentYear - +(birthday.slice(-4, birthday.length));
        },

        getPassword() {
           return this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4, birthday.length);
        },

    };

    newUser.getLogin = () => {
        let login = `${newUser.firstName[0]}${newUser.lastName}`;
        login = login.toLowerCase();
        return login
    };
    return newUser;
}

let user1 = CreateNewUser();
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());

